FROM nginx:1.27-alpine

ARG USER_ID=10001
ARG GROUP_ID=10001

RUN apk --no-cache add shadow && groupmod -g "$GROUP_ID" nginx && usermod -u "$USER_ID" -g "$GROUP_ID" nginx

COPY --chmod=0755 --chown=nginx:nginx ./static /var/www

WORKDIR /etc/nginx
COPY --chmod=0755 --chown=nginx:nginx ./nginx/default.conf /etc/nginx/conf.d/default.conf

## add permissions for nginx user
RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid &&\
        chown -R nginx:nginx /var/cache/nginx && \
        chmod -R 0755 /var/cache/nginx && \
        chown -R nginx:nginx /var/log/nginx && \
        chmod -R 0755 /var/log/nginx && \
        chown -R nginx:nginx /etc/nginx/conf.d && \
        chmod -R 0755 /etc/nginx/conf.d && \
        chown -R nginx:nginx /var/www && \
        chmod -R 0755 /var/www

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# non-root users are not allowed to listen on priviliged ports
RUN sed -i.bak 's/listen\(.*\)80;/listen 8008;/' /etc/nginx/conf.d/default.conf
EXPOSE 8008

# comment user directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

HEALTHCHECK none

USER nginx

CMD ["nginx"]