// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process/pkg/model"
)

type ServiceDiscoveryClient struct {
	url string
}

func NewServiceDiscoveryClient(url string) *ServiceDiscoveryClient {
	return &ServiceDiscoveryClient{
		url: url,
	}
}

func (s *ServiceDiscoveryClient) Fetch() (*model.DiscoveredServices, error) {
	request, err := http.NewRequest(http.MethodGet, s.url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create fetch request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving fetch: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	discoveredServices := model.DiscoveredServices{}
	err = json.Unmarshal(body, &discoveredServices)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &discoveredServices, nil
}
