// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import "gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process/pkg/model"

type ServiceDiscoveryRepository interface {
	Fetch() (*model.DiscoveredServices, error)
}
