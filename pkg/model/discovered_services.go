// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type DiscoveredServices struct {
	SessionApi               SessionApi               `json:"sessionApi"`
	FinancialClaimRequestApi FinancialClaimRequestApi `json:"financialClaimRequestApi"`
	RegistrationApi          RegistrationApi          `json:"registrationApi"`
	AppManagerUi             string                   `json:"appManagerUi"`
}

type FinancialClaimRequestApi struct {
	V3 string `json:"v3"`
}

type SessionApi struct {
	V1 string `json:"v1"`
}

type RegistrationApi struct {
	V1 string `json:"v1"`
}
